import ldap3
from flask_wtf import Form
from wtforms import TextField, PasswordField
from wtforms.validators import InputRequired
from iaasapp import db, app
 
 
def get_ldap_connection():
    conn = ldap3.initialize(app.config['LDAP_PROVIDER'])
    return conn

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(100))
 
    def __init__(self, username, password):
        self.username = username
        self.password = password
 
    @staticmethod
    def try_login(username, password):
        server = ldap3.Server(app.config['LDAP_PROVIDER'], port=389, get_info=ldap3.ALL)
        conn = ldap3.Connection(server, user=username, password=password, check_names=True, lazy=False, raise_exceptions=True)
        conn.open()
        conn.bind()
 
    def is_authenticated(self):
        return True
 
    def is_active(self):
        return True
 
    def is_anonymous(self):
        return False
 
    def get_id(self):
        return str(self.id)
 
 
class LoginForm(Form):
    username = TextField('Username', [InputRequired()])
    password = PasswordField('Password', [InputRequired()])