from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
 
 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
app.config['WTF_CSRF_SECRET_KEY'] = 'asecretkey'
app.config['LDAP_PROVIDER'] = '192.168.0.153'
app.config['LDAP_PROTOCOL_VERSION'] = 3
app.config['VCSA_ADDRESS'] = '192.168.0.155'
app.config['CUSTOMER'] = 'Cloudis Consulting'
db = SQLAlchemy(app)
 
app.secret_key = 'verysecretkey'
 
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'
 
from iaasapp.auth.views import auth
from iaasapp.vmoperations.views import vmoperations
app.register_blueprint(auth)
app.register_blueprint(vmoperations)

db.create_all()