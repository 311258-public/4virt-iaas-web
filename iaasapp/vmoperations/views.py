from flask import request, render_template, flash, redirect, \
    url_for, Blueprint, g
from flask_login import current_user, login_required
from .npiaasvmware import *
from iaasapp import app

vmoperations = Blueprint('vmoperations', __name__)

@vmoperations.route('/vmlist')
@login_required
def vmlist():
    handler = npiaasvmware(server=app.config['VCSA_ADDRESS'],
                            username=current_user.username + '@cloudisatger.lan',
                            password=current_user.password,
                            customer=app.config['CUSTOMER'])
    try:
        result = handler.listVM()
    except customerNotFound:
        written = "Customer {} not found".format(app.config['CUSTOMER'])
    else:
        written = ""
        for vm in result:
            written += "\n\n"
            written += "Name       : {}\n".format(vm['name'])
            written += "Template   : {}\n".format(vm['template'])
            written += "OS         : {}\n".format(vm['os'])
            written += "State      : {}\n".format(vm['state'])
            written += "VMTools    : {}\n".format(vm['vmtools'])
            written += "IP         : {}\n".format(vm['ip'])
            written += "Last bkp   : {}\n".format(vm['lastsnap'])

    return render_template('vmlist.html', list=written, customer=app.config['CUSTOMER'])

@vmoperations.route('/vmrestart')
@login_required
def vmrestart():
    return render_template('vmoperation.html')

@vmoperations.route('/vmdelete')
@login_required
def vmdelete():
    return render_template('vmoperation.html')